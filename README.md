# Moriarty Matrix


<p align="center">
  <img width="460" height="470" src="https://github.com/Fiery-Warrior/mm_pythonweb/blob/main/READmeSources/Images/22.png">
</p>





## Table of Contents


- [Dependencies](#dependencies)
- [Feedback](#feedback)
- [Features](#features)
- [Labs](#labs)
- [Credits](#credits)
- [License](#license)



## Description

- What is the motivation for this project? 
  - To create awareness of the hacking-lifecycle so it better prepares students, employees, cyber-enthusiasts, and security testers. 
  
- Currently optimized for window sizes between 1095px and 1535px

- Why did I build this project?
  - To provide a sleek web-based UI and UX instead of the usual command line interface.  Moriarty’s Matrix, allows a smooth transition in to this field, provides numerous tools, explains uses, and allows the user to be the demonstrator of the possible attack. Moreso, because of the relatable UI of Moriarty’s Matrix, it allows people with little cyber experience to understand and perform attacks (for educational and training purposes only). 
  
- What problem does it solve?
  - For less technical audinces it can be be difficult to use the command line interface.
  - Addresses lack of understanding of the hacking lifecycle, particularly among those who are new to the field. 
    - Recon
    - Weapon
    - Delivery
    - Exploit
    - Install
    - Control
    - Actions on Objective
  
- Who is the audience?
  - Universities & Colleges 
  - Students
  - Professors
  - Companies (Demoing and allowing hands on explanations to business partners and fellow workers)
  - Cyber-enthusiasts

- Is an offensive-focused educational ethical-hacking web-app (localhost) experience based on Lockheed Martin’s Cyber Kill Chain

- How to start:
  - Got to: \mm_pythonweb> python manage.py runserver
  
- Reminders:
  - For educational purposes only (any mis-uses of this project is ONLY the user's fault)


### Feedback
- Please provide ANY and All feedback and suggestions you have on this project. Please reach out to me at: FieryWarrior@proton.me

## Features


[![Watch the video](https://github.com/Fiery-Warrior/mm_pythonweb/blob/main/READmeSources/Images/cover.png)](https://clipchamp.com/watch/PUaV9L6zOLF)


- Walk video of entire Pub site: https://clipchamp.com/watch/PUaV9L6zOLF



https://user-images.githubusercontent.com/123214637/233187465-aced0448-2bb1-406a-b5d2-ee8626df8958.mp4



https://user-images.githubusercontent.com/123214637/233187473-137313e7-efa6-4c23-8f09-77fdc001127b.mp4


https://user-images.githubusercontent.com/123214637/233187464-4288c009-8e64-4dec-b665-78b1342ecc47.mp4


https://user-images.githubusercontent.com/123214637/233187460-1d4d2494-410b-4023-83e7-d850918d0a8f.mp4





https://user-images.githubusercontent.com/123214637/233686845-1064fd3c-a97d-4e61-9f1f-e59d669b3871.mp4



## Dependencies




```python Django```

[Sherlock-Project](https://github.com/sherlock-project/sherlock)

```pip install requests```

```Reactjs```

```npm install react-scripts --save```

```npm i react-router-dom```

```npm install @mui/material @emotion/react @emotion/styled```

```npm install react-icons --save```

## Labs
- Lab 1: Reverse Shell 
- Lab 2: Keylogger
- Lab 3: Ransomware

## Credits

Uses [Sherlock-Project](https://github.com/sherlock-project/sherlock) for one Reconanacience Stage


## License

MIT Free use

---

